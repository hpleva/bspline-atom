# bspline-atom

Relativistic Dirac-Fock atomic solver that uses B-splines as the basis functions.
